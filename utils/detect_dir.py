
# coding: utf-8


import skimage
import urllib3 as urllib
import numpy as np
from skimage import io, transform
import os
import cv2
import sys
import shutil
import glob
import pandas as pd
import xml.etree.ElementTree as ET
import tensorflow as tf
from collections import defaultdict, Counter
from io import StringIO
from matplotlib import pyplot as plt
from PIL import Image
import json
import bi2i_cv

sys.path.append('src/research/')
sys.path.append('src/research/slim/')



class MyEncoder(json.JSONEncoder):
    """Customized encoder for serialization of a dictionary
    """
def default(self,obj):
	if isinstance(obj, np.integer):
		return int(obj)
	elif isinstance(obj, np.floating):
	    	return float(obj)
	elif isinstance(obj, np.ndarray):
	    	return obj.tolist()
	else:
	    	return super(MyEncoder, self).default(obj)

def write_json(data, name):
	with open(name+'.json', 'w') as outfile:
		json.dump(data, outfile, cls = MyEncoder)

#import urllib2.

checkpoint = sys.argv[2]
model_path = sys.argv[4] #'data/'
graph_path = 'object_detection_graph_'+checkpoint
print('Graph path: {}'.format(graph_path))
folder = sys.argv[1] # 'holdout'
gt_path = model_path+folder+'.csv'
input_dir = 'images/'+folder+'/'
result_dir = 'results/'+folder+'/'
iou_threshold = float(sys.argv[3]) #.3
conf_threshold = float(sys.argv[3]) #.3
boxes_name = checkpoint+'_'+folder+'_detected_boxes'
PATH_TO_LABELS = os.path.join(model_path, 'label_map.pbtxt')
NUM_CLASSES = 9
IMAGE_SIZE = (12, 12)

from utils import label_map_util
from utils import visualization_utils as vis_util
label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
category_index = label_map_util.create_category_index(categories)

classes = [x['name'] for x in categories]

def class_text_to_int(row_label, classes=classes, categories=categories):
    if row_label in classes:
        for label in categories:
            if row_label==label['name']:
                return(label['id'])
    else:
        return(None)
def class_int_to_text(row_label, NUM_CLASSES=NUM_CLASSES, categories=categories):
    if row_label <= NUM_CLASSES:
        for label in categories:
            if row_label==label['id']:
                return(label['name'])
    else:
        return(None)





# Path to frozen detection graph. This is the actual model that is used for the object detection.
PATH_TO_CKPT = graph_path+'/frozen_inference_graph.pb'

# List of the strings that is used to add correct label for each box.
PATH_TO_LABELS = model_path+'label_map.pbtxt'
print(PATH_TO_LABELS)


detection_graph = tf.Graph()
with detection_graph.as_default():
  od_graph_def = tf.GraphDef()
  with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
    serialized_graph = fid.read()
    od_graph_def.ParseFromString(serialized_graph)
    tf.import_graph_def(od_graph_def, name='')

label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
category_index = label_map_util.create_category_index(categories)
print(categories)
def load_image_into_numpy_array(image):
  (im_width, im_height) = image.size
  return np.array(image.getdata()).reshape(
      (im_height, im_width, 3)).astype(np.uint8)




def detect_items_dir(input_dir, output_dir, show = False, save_local = True):
    TEST_IMAGE_PATHS = [ os.path.join(input_dir, '{}'.format(i)) for i in os.listdir(input_dir)]
    results = {}
    with detection_graph.as_default():
        with tf.Session(graph=detection_graph) as sess:
            # Definite input and output Tensors for detection_graph
            image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
            # Each box represents a part of the image where a particular object was detected.
            detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
            # Each score represent how level of confidence for each of the objects.
            # Score is shown on the result image, together with the class label.
            detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
            detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')
            num_detections = detection_graph.get_tensor_by_name('num_detections:0')
            for image_path in TEST_IMAGE_PATHS:
                print("scoring {}".format(image_path))
                image = Image.open(image_path).convert('RGB')
                # the array based representation of the image will be used later in order to prepare the
                # result image with boxes and labels on it.
                image_np = load_image_into_numpy_array(image)
                # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
                image_np_expanded = np.expand_dims(image_np, axis=0)
                # Actual detection.
                (boxes, scores, classes, num) = sess.run([detection_boxes, detection_scores, detection_classes, num_detections],\
                feed_dict={image_tensor: image_np_expanded})
                #item_flags_df = pd.concat([item_flags_df, items_flag(classes, scores)])
                res_ixs = scores >= conf_threshold
#                print(res_ixs.sum())
                result_dict = {'boxes': boxes[res_ixs],
                               'classes': [class_int_to_text(x) for x in classes[res_ixs]], 
                               'scores': scores[res_ixs]}
                print(result_dict['scores'])
                n_detections = len(result_dict['scores'])
                if n_detections > 0:
                    results.update({image_path.split('/')[-1]: result_dict})
                    # Visualization of the results of a detection.
                    women_count = len(scores[res_ixs])
                    vis_util.visualize_boxes_and_labels_on_image_array(
                        image_np,
                        np.squeeze(boxes),
                        np.squeeze(classes).astype(np.int32),
                        np.squeeze(scores),
                        category_index,
                        use_normalized_coordinates=True,
                        max_boxes_to_draw = 60,
                        line_thickness=2,
                        min_score_thresh = conf_threshold)
                    if show == True:
                        plt.figure(figsize=IMAGE_SIZE)
                        plt.imshow(image_np)
                    if save_local == True:
                        cv2.putText(image_np,'# detections: '+str(women_count), (10, image_np.shape[0]-10), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 3)
                        skimage.io.imsave(output_dir+image_path.split('/')[-1], image_np)
                else:
                    print('No detections!')
    return(results)


print('Scoring...')
results = detect_items_dir(input_dir, result_dir)
bi2i_cv.write_json(results, boxes_name)
print('saved detections.')
