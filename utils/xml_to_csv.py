### Convert Bounding Box Annotations from XML files to csv format


# load libraries
import skimage
import urllib3 as urllib
import numpy as np
from skimage import io, transform
import os, cv2, sys
import shutil
import glob
import pandas as pd
import xml.etree.ElementTree as ET
import tensorflow as tf
from collections import defaultdict
from io import StringIO
from matplotlib import pyplot as plt
from PIL import Image
import urllib3.request

dataset = sys.argv[1]
# environment parameters
root = os.getcwd()
imagePath = os.path.join(root,'dataset',dataset,'images')
labelsPath = os.path.join(root,'dataset',dataset, 'labels')
#linksPath = os.path.join(imagePath, 'imageLinks')
trainPath = os.path.join(labelsPath, 'train')
testPath = os.path.join(labelsPath, 'test')
samplePath = os.path.join(labelsPath, 'sample')
os.chdir(root)

# create a csv file with all the bounding box annotations from multiple XML files
# this can be directly used for tensorflow object detection API for FasterRCNN and SSD

def xml_to_csv(path):
    xml_list = []
    for xml_file in glob.glob(path + '/*.xml'):
        print(xml_file)
        tree = ET.parse(xml_file)
        root = tree.getroot()
        for member in root.findall('object'):
            value = (root.find('filename').text,
                     int(root.find('size')[0].text),
                     int(root.find('size')[1].text),
                     member[0].text,
                     int(member[4][0].text),
                     int(member[4][1].text),
                     int(member[4][2].text),
                     int(member[4][3].text)
                     )
            xml_list.append(value)
    column_name = ['filename', 'width', 'height', 'class', 'xmin', 'ymin', 'xmax', 'ymax']
    xml_df = pd.DataFrame(xml_list, columns=column_name)
    return xml_df


def main():
    for i in [trainPath, testPath]: # can pass multiple values that are defined in env parameters, e.g, [trainPath, testPath,holdoutPath]
        image_path = i
        print(image_path)
        folder = os.path.basename(os.path.normpath(i))
        print(folder)
        xml_df = xml_to_csv(image_path)
        xml_df.to_csv(folder+'.csv', index=None)
        print('Successfully converted xml to csv.')

main()
