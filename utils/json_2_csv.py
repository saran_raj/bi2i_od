#!/usr/bin/env python
# coding: utf-8

import sys
import json
import pandas as pd
import numpy as np

detection_file = sys.argv[1]

with open(detection_file+'.json', 'r') as f:
    dat = json.load(f)


res = pd.DataFrame()
for i in dat:
#     print(i)
    temp_df = pd.DataFrame()
    dsize = len(dat[i]['boxes'])
    #if dsize > 0:
    temp_df['filename'] = [i]*dsize
    temp_df['boxes'] = dat[i]['boxes']
    temp_df['class'] = dat[i]['classes']
    temp_df['score'] = dat[i]['scores']
    res = pd.concat([res, temp_df], axis = 0)


print(res.shape)

res.to_csv(detection_file+'.csv', index = False)
