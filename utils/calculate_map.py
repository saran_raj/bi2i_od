
# coding: utf-8


import skimage
import urllib3 as urllib
import numpy as np
from skimage import io, transform
import os
import cv2
import sys
import shutil
import glob
import pandas as pd
import xml.etree.ElementTree as ET
import tensorflow as tf
from collections import defaultdict, Counter
from io import StringIO
from matplotlib import pyplot as plt
from PIL import Image
import json
import bi2i_cv

sys.path.append('models/research/')
sys.path.append('models/research/slim/')

boxes_name = sys.argv[2]
gt_path = sys.argv[1]
iou_threshold = float(sys.argv[3])
print(iou_threshold)
class MyEncoder(json.JSONEncoder):
    """Customized encoder for serialization of a dictionary
    """
def default(self,obj):
	if isinstance(obj, np.integer):
		return int(obj)
	elif isinstance(obj, np.floating):
	    	return float(obj)
	elif isinstance(obj, np.ndarray):
	    	return obj.tolist()
	else:
	    	return super(MyEncoder, self).default(obj)

def write_json(data, name):
	with open(name+'.json', 'w') as outfile:
		json.dump(data, outfile, cls = MyEncoder)


restr_df= pd.read_csv(boxes_name)
print(restr_df.head())
print(restr_df.shape)
classes = restr_df['class'].unique().tolist()
restr_df['Box_Values'] = [x.replace('[', '').replace(']','').split(',') for x in restr_df.boxes]
print(restr_df.dtypes)
# loading and processing ground truth
actual_tr = pd.read_csv(gt_path)
#actual_tr = actual_tr[actual_tr.filename.isin(restr_df.filename.unique().tolist())]
actual_tr.reset_index(inplace=True, drop=True)
actual_tr['xmin'] = [x/float(y) for x,y in zip(actual_tr['xmin'], actual_tr['width'])]
actual_tr['xmax'] = [x/float(y) for x,y in zip(actual_tr['xmax'], actual_tr['width'])]
actual_tr['ymin'] = [x/float(y) for x,y in zip(actual_tr['ymin'], actual_tr['height'])]
actual_tr['ymax'] = [x/float(y) for x,y in zip(actual_tr['ymax'], actual_tr['height'])]
actual_tr.shape
print(actual_tr['class'].value_counts())
print(actual_tr.describe())

# pre processing to calculate mAP
dets = []
for i in range(restr_df.shape[0]):
    img_name = restr_df['filename'][i]
    bbox = [float(x) for x in restr_df['Box_Values'][i]]
    clas = restr_df['class'][i]
    score = restr_df['score'][i]
    dets.append([img_name, clas, score, bbox])

gts = []
for i in range(actual_tr.shape[0]):
    img_name = actual_tr['filename'][i]
    bbox = [actual_tr['ymin'][i], actual_tr['xmin'][i], actual_tr['ymax'][i], actual_tr['xmax'][i]]
    # [actual_tr['xmax'][i], actual_tr['xmin'][i], actual_tr['ymax'][i], actual_tr['ymin'][i]]
    clas = actual_tr['class'][i]
    score = 1
    gts.append([img_name, clas, score, bbox])

#classes =  ['bottle', 'car', 'motorcycle', 'chair', 'person', 'laptop'] #['person', 'chair', 'bottle']

def boxesIntersect(boxA, boxB):
    if boxA[0] > boxB[2]: 
        return False # boxA is right of boxB
    if boxB[0] > boxA[2]:
        return False # boxA is left of boxB
    if boxA[3] < boxB[1]:
        return False # boxA is above boxB
    if boxA[1] > boxB[3]:
        return False # boxA is below boxB
    return True

def getIntersectionArea(boxA, boxB):
    xA = max(boxA[0], boxB[0])
    yA = max(boxA[1], boxB[1])
    xB = min(boxA[2], boxB[2])
    yB = min(boxA[3], boxB[3])
    # intersection area
    return (xB - xA + 1) * (yB - yA + 1)

def getArea(box):
    return (box[2] - box[0] + 1) * (box[3] - box[1] + 1)

def getUnionAreas(boxA, boxB, interArea=None):
    area_A = getArea(boxA)
    area_B = getArea(boxB)
    if interArea == None:
        interArea = getIntersectionArea(boxA, boxB)
    return float(area_A + area_B - interArea)


def iou(boxA, boxB):
    # if boxes dont intersect
    if boxesIntersect(boxA, boxB) == False:
        return 0
    interArea = getIntersectionArea(boxA,boxB)
    union = getUnionAreas(boxA,boxB,interArea=interArea)
    # intersection over union
    iou = interArea / union
    assert iou >= 0
    return iou

def CalculateAveragePrecision(rec, prec):
    mrec = []
    mrec.append(0)
    [mrec.append(e) for e in rec]
    mrec.append(1)
    mpre = []
    mpre.append(0)
    [mpre.append(e) for e in prec]
    mpre.append(0)
    for i in range(len(mpre)-1, 0, -1):
        mpre[i-1]=max(mpre[i-1],mpre[i])
    ii = []
    for i in range(len(mrec)-1):
        if mrec[1:][i]!=mrec[0:-1][i]:
            ii.append(i+1)
    ap = 0
    for i in ii:
        ap = ap + np.sum((mrec[i]-mrec[i-1])*mpre[i])
    # return [ap, mpre[1:len(mpre)-1], mrec[1:len(mpre)-1], ii]
    return [ap, mpre[0:len(mpre)-1], mrec[0:len(mpre)-1], ii]


def get_precion_recall_per_class(dets, gts, classes, iou_threshold):
    ret = []
    for c in classes:
        #c = classes[0]
        print(c)
        dects = []
        [dects.append(d) for d in dets if d[1] == c]
        grts = []
        [grts.append(g) for g in gts if g[1] == c]
        npos = len(grts)
        dects = sorted(dects, key=lambda conf: conf[2], reverse=True)
        TP = np.zeros(len(dects))
        FP = np.zeros(len(dects))
        det = Counter([cc[0] for cc in gts])
        for key,val in det.items():
            det[key] = np.zeros(val)
        max_iou = sys.float_info.min
        
        for d in range(len(dects)):
            # print('dect %s => %s' % (dects[d][0], dects[d][3],))
            # Find ground truth image
            gt = [gt for gt in grts if gt[0] == dects[d][0]]
            iouMax = sys.float_info.min
            ngt = len(gt)
            if ngt > 0:
                for j in range(ngt):
                    # print('Ground truth gt => %s' % (gt[j][3],))
                    iou_val = iou(dects[d][3], gt[j][3])
                    if iou_val>iouMax:
                        iouMax=iou_val
                        jmax=j
                # Assign detection as true positive/don't care/false positive
                if iouMax>=iou_threshold:
                    if det[dects[d][0]][jmax] == 0:
                        TP[d]=1  # count as true positive
                        # print("TP")
                    det[dects[d][0]][jmax]=1 # flag as already 'seen'
                # - A detected "cat" is overlaped with a GT "cat" with IOU >= IOUThreshold.
                else:
                    FP[d]=1 # count as false positive
                    # print("FP")
            else:
                    FP[d]=1 # count as false positive
                    # print("FP")
        acc_FP=np.cumsum(FP)
        acc_TP=np.cumsum(TP)
        rec=acc_TP/npos
        prec=np.divide(acc_TP,(acc_FP+acc_TP))
        [ap, mpre, mrec, ii] = CalculateAveragePrecision(rec, prec)
        r = {
            'class': c,
            'precision' : prec,
            'recall': rec,
            'AP': ap,
            'interpolated precision': mpre,
            'interpolated recall': mrec,
            'total positives': npos,
            'total TP': np.sum(TP),
            'total FP': np.sum(FP)                
            }
        ret.append(r)
    print("FINAL")
    print(ret)
    return(ret)

def CalculateMeanAveragePrecision():
    aps = {}
    acc_AP = 0
    validClasses = 0
    for metricsPerClass in metricsPerClasses:
        cl = metricsPerClass['class']
        ap = metricsPerClass['AP']
        aps.update({cl:ap})
        precision = metricsPerClass['precision']
        recall = metricsPerClass['recall']
        totalPositives = metricsPerClass['total positives']
        total_TP = metricsPerClass['total TP']
        total_FP = metricsPerClass['total FP']
        if totalPositives > 0:
            validClasses = validClasses + 1
            acc_AP = acc_AP + ap
            prec = ['%.2f'% p for p in precision]
            rec = ['%.2f'% r for r in recall]
            ap_str = "{0:.2f}%".format(ap*100)
            # ap_str = str('%.2f' % ap) #AQUI
            print('AP: %s (%s)' % (ap_str, cl))

    mAP = acc_AP/validClasses
    mAP_str = "{0:.2f}%".format(mAP*100)
    print('mAP: %s' % mAP_str)
    return(aps, mAP)


metricsPerClasses = get_precion_recall_per_class(dets, gts, classes, iou_threshold = iou_threshold)
CalculateMeanAveragePrecision()


