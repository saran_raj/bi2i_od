
# coding: utf-8
# Author: Saranraj Dhatchinamoorthy
# Title : Predictions to Annotations
# Description: Create xml files from the predictions of the current in AL pipeline. These xml files can be helpful for effective annotation as it draws the  temporary bboxes based on the prediction, the oracle then, have to just correct/del/add the bboxes.

# load required packages
import sys
import skimage
import urllib3 as urllib
import numpy as np
from skimage import io, transform
import os
import shutil
import glob
import pandas as pd
import xml.etree.ElementTree as ET
import tensorflow as tf
from collections import defaultdict
from io import StringIO
from matplotlib import pyplot as plt
from PIL import Image
import urllib3.request

# arg parsing
sample_dir = sys.argv[1]
predictions_filename = sys.argv[2]+'_sampledetected_boxes.csv'

# load predictions of sampled instances for this iteration
samples_pred = pd.read_csv(predictions_filename)
sampled_images = os.listdir(sample_dir)
# keep only most uncertain instances
samples_pred = samples_pred[samples_pred.filename.isin(sampled_images)]
n_predictions = samples_pred.shape[0]
print(n_predictions)

# processing
samples_pred['boxes'] = [x.replace('[', '').replace(']', '') for x in samples_pred.boxes]

# creating xml for each predictions and saves in the same folder

for filename in samples_pred.filename.values:
#    print('creating xml from csv for {}'.format(filename))
    predictions = samples_pred[samples_pred.filename == filename]
    predictions.shape
    predictions.reset_index(inplace=True, drop=True)
#    print(predictions.shape[0])
    annotation = ET.Element("annotation")
    image = Image.open(os.path.join(sample_dir, filename))
    img_w, img_h = image.size
    ET.SubElement(annotation, "folder").text = sample_dir
    ET.SubElement(annotation, "filename").text = filename
    size = ET.SubElement(annotation, 'size')
    ET.SubElement(size, 'width').text = str(img_w)
    ET.SubElement(size, 'height').text = str(img_h)
    ET.SubElement(size, 'depth').text = '3'
    ET.SubElement(annotation, 'segmented').text = '0'
    for i in range(predictions.shape[0]):
        object = ET.SubElement(annotation, 'object')
        ET.SubElement(object, 'name').text = predictions.iloc[i]['class']
        bndbox = ET.SubElement(object, 'bndbox')
        xmin, ymin, xmax, ymax = [float(x) for x in predictions.loc[i]['boxes'].split(',')]
        ET.SubElement(bndbox, "xmin").text = str(int(ymin*img_w))
        ET.SubElement(bndbox, "ymin").text = str(int(xmin*img_h))
        ET.SubElement(bndbox, "xmax").text = str(int(ymax*img_w))
        ET.SubElement(bndbox, "ymax").text = str(int(xmax*img_h))
    tree = ET.ElementTree(annotation)
    tree.write(os.path.join(sample_dir, filename.replace('.jpg','.xml')), xml_declaration=True, encoding='utf-8')
print('Successfully done for {} predictions'.format(n_predictions))
