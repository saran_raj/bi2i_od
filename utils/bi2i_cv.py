# coding: utf-8

import requests
import pandas as pd
import numpy as np
import imutils
import json

class MyEncoder(json.JSONEncoder):
    """Customized encoder for serialization of a dictionary
    """
    def default(self,obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        else:
            return super(MyEncoder, self).default(obj)

def write_json(data, name):
    with open(name+'.json', 'w') as outfile:
        json.dump(data, outfile, cls = MyEncoder)


def result2csv(results, res_name, no_count = True):
    df = pd.DataFrame.from_dict(results,orient='index')
    df['zipped'] = list(zip(df.boxes.values, df.scores.values))
    df['var1'] = df['zipped'].apply(lambda x:list(zip(*x)))
    s = df.apply(lambda x: pd.Series(x['var1']),axis=1).stack().reset_index(level=1, drop=True)
    s.name = 'changed_var'
    df_new=df.drop('var1', axis=1).join(s)
    nona=df_new[~df_new.changed_var.isnull()]
    nona['Boxes_Values'] = nona['changed_var'].apply(lambda x:x[0])
    nona['scores'] = nona['changed_var'].apply(lambda x:x[1])
    #nona['scores'] = nona['changed_var'].apply(lambda x:x[2])
    nona['filename'] = nona.index
    nona.reset_index(drop=True, inplace=True)
    nona=nona[["scores","filename","Boxes_Values"]]
    res = get_counts(nona, res_name)
    if no_count:
        return(nona)
    else:
        withna=pd.DataFrame({"Image":df_new[df_new.changed_var.isnull()].index.unique().values, res_name:0})
        final=res.append(withna,ignore_index=True)
        return(final)
#def result2csv(results, no_count = True):
#    df = pd.DataFrame.from_dict(results,orient='index')
#    df['zippeed'] = list(zip(df['items'].values , df['boxes'].values, df['scores'].values))
#    df['var1'] = df['zippeed'].apply(lambda x:list(zip(*x)))
#    s = df.apply(lambda x: pd.Series(x['var1']),axis=1).stack().reset_index(level=1, drop=True)
#    s.name = 'changed_var'
#    df_new=df.drop('var1', axis=1).join(s)
#    nona=df_new[~df_new.changed_var.isnull()]
#    nona['class'] = nona['changed_var'].apply(lambda x:x[0])
#    nona['Boxes_Values'] = nona['changed_var'].apply(lambda x:x[1])
#    nona['scores'] = nona['changed_var'].apply(lambda x:x[2])
#    nona['filename'] = nona.index
#    nona.reset_index(drop=True, inplace=True)
#    nona=nona[["scores","filename","class","Boxes_Values"]]
#    if no_count:
#        return(nona)
#    else:
#        withna=pd.DataFrame({"filename":df_new[df_new.changed_var.isnull()].index.values,"scores":0,"class":0,"Boxes_Values":0})
#        final=nona.append(withna,ignore_index=True)
#        return(final)
#

def calculate_error(a, p, c):
    if (p >= a) & (p <=c):
        return(0)
    else:
        return(min(abs(p-a), abs(c-p)))

def load_image_into_numpy_array(image):
  (im_width, im_height) = image.size
  return np.array(image.getdata()).reshape(
      (im_height, im_width, 3)).astype(np.uint8)

def rotate_img(img,p,theta=90):
    if p>=0.85:
        rotated = imutils.rotate_bound(image, theta)
        cv2.imwrite("hor_images/"+img.split('/')[-1],rotated)
    else:
        cv2.imwrite("hor_images/"+img.split('/')[-1],image)

def get_counts(detections, name):
    women_count_df = pd.DataFrame(detections.filename.value_counts())
    women_count_df.reset_index(inplace=True)
    women_count_df.columns = ['Image', name]
    return(women_count_df)

def get_count_band(detections,lowp, highp, actp):
    a = get_counts(detections[detections.scores >= lowp], 'high')
    b = get_counts(detections[detections.scores >= actp], 'medium')
    b = b.merge(a, on = 'Image')
    c = get_counts(detections[detections.scores >= highp], 'low')
    c = c.merge(b, on='Image')
    return(c)



def women_count_accuracy(actual, predicted, max_diff = 2):
    errors = abs(actual-predicted)
    tp = np.sum(errors <= max_diff)
    return(tp/float(len(errors)))

def calculate_error(a, p, c):
    if (p >= a) & (p <=c):
        return(0)
    else: 
        return(min(abs(p-a), abs(c-p)))


def download_images_from_links(image_links, output_dir):
    print('downloading {} images...'.format(len(image_links)))
    for url in image_links:
        filename = '_'.join(url.split('/')[-2:])
        r = requests.get(url, allow_redirects=True)
        open(output_dir+'/'+filename, 'wb').write(r.content)


