
# coding: utf-8


import skimage
import urllib3 as urllib
import numpy as np
from skimage import io, transform
import os
import cv2
import sys
import shutil
import glob
import pandas as pd
import xml.etree.ElementTree as ET
import tensorflow as tf
from collections import defaultdict, Counter
from io import StringIO
from matplotlib import pyplot as plt
from PIL import Image
import json
import bi2i_cv

sys.path.append('models/research/')
sys.path.append('models/research/slim/')


class MyEncoder(json.JSONEncoder):
    """Customized encoder for serialization of a dictionary
    """
def default(self,obj):
	if isinstance(obj, np.integer):
		return int(obj)
	elif isinstance(obj, np.floating):
	    	return float(obj)
	elif isinstance(obj, np.ndarray):
	    	return obj.tolist()
	else:
	    	return super(MyEncoder, self).default(obj)

def write_json(data, name):
	with open(name+'.json', 'w') as outfile:
		json.dump(data, outfile, cls = MyEncoder)

#import urllib2.

checkpoint = sys.argv[2]
model_path = sys.argv[4] #'data/'
graph_path = 'object_detection_graph_'+checkpoint
print('Graph path: {}'.format(graph_path))
folder = sys.argv[1] # 'holdout'
gt_path = model_path+folder+'.csv'
input_dir = 'images/'+folder+'/'
result_dir = 'results/'+folder+'/'
iou_threshold = float(sys.argv[3]) #.3
conf_threshold = float(sys.argv[3]) #.3
boxes_name = checkpoint+'_'+folder+'_detected_boxes'

def class_text_to_int(row_label):
    if row_label == 'woman':
        return 1
#    elif row_label == 'KS':
#        return 2
#    elif row_label == 'BT':
#        return 3
#    elif row_label == 'WB':
#        return 4
#    elif row_label == 'SD_SDCO':
#        return 5
#    elif row_label == 'TS':
#        return 6
#    elif row_label == 'SCS':
#        return 7
    else:
        None

def class_int_to_text(row_label):
    if row_label == 1:
        return 'woman'
#    elif row_label == 2:
#        return 'KS'
#    elif row_label == 3:
#        return 'BT'
#    elif row_label == 4:
#        return 'WB'
#    elif row_label == 5:
#        return 'SD_SDCO'
#    elif row_label == 6:
#        return 'TS'
#    elif row_label == 7:
#        return 'SCS'
    else:
        None



        
def class_to_flags(clss):
    res = [0]*NUM_CLASSES
    for i in np.unique(clss):
        #print i
        res[int(i-1)] = 1
    return(np.array(res))

def items_flag(classes, scores):
    clss = classes[scores>.5]
    clss = class_to_flags(clss)
    return(pd.DataFrame({'image' : [image_path.split('/')[-1]], 'Others' : [clss[0]]}))

from utils import label_map_util
from utils import visualization_utils as vis_util


# Path to frozen detection graph. This is the actual model that is used for the object detection.
PATH_TO_CKPT = graph_path+'/frozen_inference_graph.pb'

# List of the strings that is used to add correct label for each box.
PATH_TO_LABELS = model_path+'label_map.pbtxt'
print(PATH_TO_LABELS)
NUM_CLASSES = 8
IMAGE_SIZE = (12, 12)

detection_graph = tf.Graph()
with detection_graph.as_default():
  od_graph_def = tf.GraphDef()
  with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
    serialized_graph = fid.read()
    od_graph_def.ParseFromString(serialized_graph)
    tf.import_graph_def(od_graph_def, name='')

label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
category_index = label_map_util.create_category_index(categories)

def load_image_into_numpy_array(image):
  (im_width, im_height) = image.size
  return np.array(image.getdata()).reshape(
      (im_height, im_width, 3)).astype(np.uint8)




def detect_items_dir(input_dir, output_dir, show = False, save_local = True):
    TEST_IMAGE_PATHS = [ os.path.join(input_dir, '{}'.format(i)) for i in os.listdir(input_dir)]
    results = {}
    with detection_graph.as_default():
        with tf.Session(graph=detection_graph) as sess:
            # Definite input and output Tensors for detection_graph
            image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
            # Each box represents a part of the image where a particular object was detected.
            detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
            # Each score represent how level of confidence for each of the objects.
            # Score is shown on the result image, together with the class label.
            detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
            detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')
            num_detections = detection_graph.get_tensor_by_name('num_detections:0')
            for image_path in TEST_IMAGE_PATHS:
                print("scoring {}".format(image_path))
                image = Image.open(image_path)
                # the array based representation of the image will be used later in order to prepare the
                # result image with boxes and labels on it.
                image_np = load_image_into_numpy_array(image)
                # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
                image_np_expanded = np.expand_dims(image_np, axis=0)
                # Actual detection.
                (boxes, scores, classes, num) = sess.run([detection_boxes, detection_scores, detection_classes, num_detections],\
                feed_dict={image_tensor: image_np_expanded})
                #item_flags_df = pd.concat([item_flags_df, items_flag(classes, scores)])
                res_ixs = scores >= conf_threshold
#                print(res_ixs.sum())
                result_dict = {'boxes': boxes[res_ixs], 
                            'items': [class_int_to_text(x) for x in classes[res_ixs]], 
                            'classes': classes[res_ixs],
                            'scores': scores[res_ixs],
                            'item_flags': class_to_flags(classes[res_ixs])}
                print(result_dict['scores'])
                n_detections = len(result_dict['scores'])
                if n_detections > 0:
                    results.update({image_path.split('/')[-1]: result_dict})
                    # Visualization of the results of a detection.
                    women_count = len(scores[res_ixs])
                    vis_util.visualize_boxes_and_labels_on_image_array(
                        image_np,
                        np.squeeze(boxes),
                        np.squeeze(classes).astype(np.int32),
                        np.squeeze(scores),
                        category_index,
                        use_normalized_coordinates=True,
                        max_boxes_to_draw = 60,
                        line_thickness=2, 
                        min_score_thresh = conf_threshold)
                    if show == True:
                        plt.figure(figsize=IMAGE_SIZE)
                        plt.imshow(image_np)
                    if save_local == True:
                        cv2.putText(image_np,'# detections: '+str(women_count), (10, image_np.shape[0]-10), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 3)
                        skimage.io.imsave(output_dir+image_path.split('/')[-1], image_np)
                else:
                    print('No detections!')
    return(results)


print('Scoring...')
results = detect_items_dir(input_dir, result_dir)
bi2i_cv.write_json(results, boxes_name)
print('saved detections.')
def result2csv(results):
    df = pd.DataFrame.from_dict(results,orient='index')
    df['zippeed'] = list(zip(df['items'].values , df['boxes'].values, df['scores']))
    df['var1'] = df['zippeed'].apply(lambda x:list(zip(*x)))
    s = df.apply(lambda x: pd.Series(x['var1']),axis=1).stack().reset_index(level=1, drop=True)
    s.name = 'changed_var'
    df_new=df.drop('var1', axis=1).join(s)
    df_new['class'] = df_new['changed_var'].apply(lambda x:x[0])
    df_new['Boxes_Values'] = df_new['changed_var'].apply(lambda x:x[1])
    df_new['scores'] = df_new['changed_var'].apply(lambda x:x[2])
    df_new['filename'] = df_new.index
    df_new.reset_index(drop=True, inplace=True)
    return(df_new[['filename', 'class', 'Boxes_Values', 'scores']])

# converting results to a dataframe
restr_df = result2csv(results)
# maping with text label
# labels = label_map_util.get_label_map_dict(PATH_TO_LABELS, use_display_name=True)
# labels = dict((v,k) for k,v in labels.iteritems()) 
# restr_df['class'] = restr_df['class'].astype('int')
# restr_df['class'].replace(labels, inplace = True)
classes = restr_df['class'].unique().tolist()

restr_df.to_csv(boxes_name+'.csv', index = False)
print(restr_df.shape)
# loading and processing ground truth
actual_tr = pd.read_csv(gt_path)
#actual_tr = actual_tr[actual_tr.filename.isin(restr_df.filename.unique().tolist())]
actual_tr.reset_index(inplace=True, drop=True)
actual_tr['xmin'] = [x/float(y) for x,y in zip(actual_tr['xmin'], actual_tr['width'])]
actual_tr['xmax'] = [x/float(y) for x,y in zip(actual_tr['xmax'], actual_tr['width'])]
actual_tr['ymin'] = [x/float(y) for x,y in zip(actual_tr['ymin'], actual_tr['height'])]
actual_tr['ymax'] = [x/float(y) for x,y in zip(actual_tr['ymax'], actual_tr['height'])]
actual_tr.shape
print(actual_tr['class'].value_counts())
print(actual_tr.describe())

# pre processing to calculate mAP
dets = []
for i in range(restr_df.shape[0]):
    img_name = restr_df['filename'][i]
    bbox = restr_df['Boxes_Values'][i]
    clas = restr_df['class'][i]
    score = restr_df['scores'][i]
    dets.append([img_name, clas, score, bbox])

gts = []
for i in range(actual_tr.shape[0]):
    img_name = actual_tr['filename'][i]
    bbox = [actual_tr['ymin'][i], actual_tr['xmin'][i], actual_tr['ymax'][i], actual_tr['xmax'][i]]
    # [actual_tr['xmax'][i], actual_tr['xmin'][i], actual_tr['ymax'][i], actual_tr['ymin'][i]]
    clas = actual_tr['class'][i]
    score = 1
    gts.append([img_name, clas, score, bbox])

#classes =  ['bottle', 'car', 'motorcycle', 'chair', 'person', 'laptop'] #['person', 'chair', 'bottle']

def boxesIntersect(boxA, boxB):
    if boxA[0] > boxB[2]: 
        return False # boxA is right of boxB
    if boxB[0] > boxA[2]:
        return False # boxA is left of boxB
    if boxA[3] < boxB[1]:
        return False # boxA is above boxB
    if boxA[1] > boxB[3]:
        return False # boxA is below boxB
    return True

def getIntersectionArea(boxA, boxB):
    xA = max(boxA[0], boxB[0])
    yA = max(boxA[1], boxB[1])
    xB = min(boxA[2], boxB[2])
    yB = min(boxA[3], boxB[3])
    # intersection area
    return (xB - xA + 1) * (yB - yA + 1)

def getArea(box):
    return (box[2] - box[0] + 1) * (box[3] - box[1] + 1)

def getUnionAreas(boxA, boxB, interArea=None):
    area_A = getArea(boxA)
    area_B = getArea(boxB)
    if interArea == None:
        interArea = getIntersectionArea(boxA, boxB)
    return float(area_A + area_B - interArea)


def iou(boxA, boxB):
    # if boxes dont intersect
    if boxesIntersect(boxA, boxB) == False:
        return 0
    interArea = getIntersectionArea(boxA,boxB)
    union = getUnionAreas(boxA,boxB,interArea=interArea)
    # intersection over union
    iou = interArea / union
    assert iou >= 0
    return iou

def CalculateAveragePrecision(rec, prec):
    mrec = []
    mrec.append(0)
    [mrec.append(e) for e in rec]
    mrec.append(1)
    mpre = []
    mpre.append(0)
    [mpre.append(e) for e in prec]
    mpre.append(0)
    for i in range(len(mpre)-1, 0, -1):
        mpre[i-1]=max(mpre[i-1],mpre[i])
    ii = []
    for i in range(len(mrec)-1):
        if mrec[1:][i]!=mrec[0:-1][i]:
            ii.append(i+1)
    ap = 0
    for i in ii:
        ap = ap + np.sum((mrec[i]-mrec[i-1])*mpre[i])
    # return [ap, mpre[1:len(mpre)-1], mrec[1:len(mpre)-1], ii]
    return [ap, mpre[0:len(mpre)-1], mrec[0:len(mpre)-1], ii]


def get_precion_recall_per_class(dets, gts, classes, iou_threshold):
    ret = []
    for c in classes:
        #c = classes[0]
        print(c)
        dects = []
        [dects.append(d) for d in dets if d[1] == c]
        grts = []
        [grts.append(g) for g in gts if g[1] == c]
        npos = len(grts)
        dects = sorted(dects, key=lambda conf: conf[2], reverse=True)
        TP = np.zeros(len(dects))
        FP = np.zeros(len(dects))
        det = Counter([cc[0] for cc in gts])
        for key,val in det.items():
            det[key] = np.zeros(val)
        max_iou = sys.float_info.min
        for d in range(len(dects)):
            # print('dect %s => %s' % (dects[d][0], dects[d][3],))
            # Find ground truth image
            grts = [grts for grts in gts if grts[0] == dects[d][0]]
            iouMax = sys.float_info.min
            for j in range(len(grts)):
                # print('Ground truth gt => %s' % (gt[j][3],))
                iou_val = iou(dects[d][3], grts[j][3])
                if iou_val>iouMax:
                    iouMax=iou_val
                    jmax=j
            # Assign detection as true positive/don't care/false positive
            if iouMax>=iou_threshold:
                if det[dects[d][0]][jmax] == 0:
                    TP[d]=1  # count as true positive
                    # print("TP")
                det[dects[d][0]][jmax]=1 # flag as already 'seen'
            # - A detected "cat" is overlaped with a GT "cat" with IOU >= IOUThreshold.
            else:
                FP[d]=1 # count as false positive
                # print("FP")
        acc_FP=np.cumsum(FP)
        acc_TP=np.cumsum(TP)
        rec=acc_TP/npos
        prec=np.divide(acc_TP,(acc_FP+acc_TP))
        [ap, mpre, mrec, ii] = CalculateAveragePrecision(rec, prec)
        r = {
            'class': c,
            'precision' : prec,
            'recall': rec,
            'AP': ap,
            'interpolated precision': mpre,
            'interpolated recall': mrec,
            'total positives': npos,
            'total TP': np.sum(TP),
            'total FP': np.sum(FP)                
            }
        ret.append(r)
    return(ret)

def CalculateMeanAveragePrecision():
    aps = {}
    acc_AP = 0
    validClasses = 0
    for metricsPerClass in metricsPerClasses:
        cl = metricsPerClass['class']
        ap = metricsPerClass['AP']
        aps.update({cl:ap})
        precision = metricsPerClass['precision']
        recall = metricsPerClass['recall']
        totalPositives = metricsPerClass['total positives']
        total_TP = metricsPerClass['total TP']
        total_FP = metricsPerClass['total FP']
        if totalPositives > 0:
            validClasses = validClasses + 1
            acc_AP = acc_AP + ap
            prec = ['%.2f'% p for p in precision]
            rec = ['%.2f'% r for r in recall]
            ap_str = "{0:.2f}%".format(ap*100)
            # ap_str = str('%.2f' % ap) #AQUI
            print('AP: %s (%s)' % (ap_str, cl))

    mAP = acc_AP/validClasses
    mAP_str = "{0:.2f}%".format(mAP*100)
    print('mAP: %s' % mAP_str)
    return(aps, mAP)


metricsPerClasses = get_precion_recall_per_class(dets, gts, classes, iou_threshold = iou_threshold)
CalculateMeanAveragePrecision()


