
# Object Detection Training for Custom Dataset

Object detection in computer vision can be defined as  identifying a particular object and obtain the exact location of that object in a given image. This is widely  useful in several application in contemporary Artificial Intelligence use cases.

## Example: Use-Cases


This repository helps to train any custom defined object using set of object detection architectures. One can train a object detection architecture using images and their ground truth lable. There are few open source annotation(labelling) tool available for this purpose. We can use [labelImg](https://github.com/tzutalin/labelImg) for the annotation.
However, designing an novel architecture and train the same from scratch would need enormous amount of data and labelling will be expensive. [ImageNet](http://www.image-net.org/) and [COCO](http://cocodataset.org/) are large scale open source datasets. There are many pretrained src available for object detection using these datasets. So this pre-trained weights can be used for our custom object rather than random initialization, this will fasten the learning with our labeled images. We need approximately 500 images for decent object detection model. However there's no rule of thumb for number of images required for object detection model. If our case have many objects and long tail distribution we will probably need more images. More images better learning. 

## Environment Setup
It is always safer to use a detecated virtual envirorment. Below snippet will creat a virtual environment in ubuntu.
```{bash}
virtualenv -p python3 ~/od
pip install --upgrade pip
```
## Dependencies 
Refer requirements.txt
To install all packages at once use the following command.
```{bash}
pip install -r requirements.txt
```
The following steps will take care of the object detection training from data preparation to overall performance. The images are annotated usign [labelImg](https://github.com/tzutalin/labelImg), refer **dataset/brands** for more details.

## 1. data prep for training
When the images are labeled, we get two folders as the input for the model training, ***Images & labels***. Tensorflow recommends an optimal way to load the dataset for the training using [TFRecords](https://www.tensorflow.org/tutorials/load_data/tfrecord). [labelImg]() gives us the annotation in the xml format, we have to convert this to a *.csv* for TFRecord generation. Following snippet will conver the xml's to csv and then generate the TFRecords.

```{bash}
python xml_to_csv.py
mv train.csv test.csv data/
python generate_tfrecord.py
mv train.record test.record data/
```
### Configure the training process
As we are using the custom images and lables based on our use case, we have to change the dataset and training parameters in the architecture configuration.
 
SSD (faster training and inference): ssd_mobilenet_v1_pets.config
Faster-RCNN (for best detection but slower than ssd): faster_rcnn_inception_resnet_v2.config

```{bash}
vi data/ssd_mobilenet_v1_pets.config # for FasterRNN: faster_rcnn_inception_resnet_v2.config 
```
#### Important Parameters:
* *num_classes* - number objects to be trained for.
* *num_steps* - How many time the weight should be updated.
* *batch_size* - How many image to consider for one weight update.
###### Note:
 FasterRCNN is an image centric algorithm, each image will be used for one weight update. So batch_size should always be 1.

## 2. Training

```{bash}
python src/research/object_detection/train.py --logstderr --train_dir data/ --pipeline_config_path data/ssd_mobilenet_v1_pets.config
```
Make sure the loss is going down while training, stop the training (Ctrl+C) if the loss cosistently increasing for few steps.Once its done, last five model weights will be  saved in ***./data***.
```{bash}
ls ./data/model-ckpt* -l
```
This results as
```{bash}
-rw-rw-r-- 1 bridgei2i bridgei2i 90264808 May 26 19:05 data/model.ckpt-0.data-00000-of-00001
-rw-rw-r-- 1 bridgei2i bridgei2i    26523 May 26 19:05 data/model.ckpt-0.index
-rw-rw-r-- 1 bridgei2i bridgei2i 10110496 May 26 19:06 data/model.ckpt-0.meta
-rwxrwxr-x 1 bridgei2i bridgei2i 90264808 May 26 18:46 data/model.ckpt-1.data-00000-of-00001
-rwxrwxr-x 1 bridgei2i bridgei2i    26523 May 26 18:46 data/model.ckpt-1.index
-rwxrwxr-x 1 bridgei2i bridgei2i  9599679 May 26 18:46 data/model.ckpt-1.meta
-rw-rw-r-- 1 bridgei2i bridgei2i 90264808 May 26 19:30 data/model.ckpt-20.data-00000-of-00001
-rw-rw-r-- 1 bridgei2i bridgei2i    26523 May 26 19:30 data/model.ckpt-20.index
-rw-rw-r-- 1 bridgei2i bridgei2i 10110497 May 26 19:30 data/model.ckpt-20.meta
-rw-rw-r-- 1 bridgei2i bridgei2i 90264808 May 26 19:40 data/model.ckpt-77.data-00000-of-00001
-rw-rw-r-- 1 bridgei2i bridgei2i    26523 May 26 19:40 data/model.ckpt-77.index
-rw-rw-r-- 1 bridgei2i bridgei2i 10110497 May 26 19:40 data/model.ckpt-77.meta
```

## 3. Generate inference graph
```
python src/research/object_detection/export_inference_graph.py --input_type image_tensor --pipeline_config_path data/ssd_mobilenet_v1_pets.config --trained_checkpoint_prefix data/model.ckpt-77 --output_directory object_detection_graph_77
```
## 4. Scoring (detection on new images)
```
python detect_dir.py test 3660 .3 data/

```
## 5. Performance of the trained model
```{bash}
#convert json to csv
python json_2_csv.py 3660_test_detection_boxes
# calculate Mean Average Precision
python calculate_map.py data/test.csv 3660_test_boxes.csv
```
This will give Average Precision for all classes and overall Mean Average Precision, as below.
```{bash}
AP: 87.59% (shine)
AP: 24.61% (roosh)
AP: 64.88% (fem)
AP: 3.97% (teebay)
AP: 24.60% (smug)
AP: 8.90% (colin)
AP: 15.01% (odonil)
AP: 0.73% (bisleri)
AP: 5.21% (glife)
mAP: 26.17%
```
